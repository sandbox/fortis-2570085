<?php

/**
 * @file
 * Contains the main purging functionality and error handling
 */

/**
 * Purges urls from Airee cache.
 *
 * @param array $airee_urls
 *   Array of urls to purge from Airee cache using secure POST method.
 *
 * @return array
 *   Array of urls and their http status codes after purging.
 */
function airee_urls($airee_urls) {

  static $url_cache;
  $urls = array();

  // Initialize static URL array, used for preventing duplicate purges.
  if (!isset($url_cache)) {
    $url_cache = array();
  }

  // Info required for each purge request.
  $params = array();
  $params['key'] = variable_get('airee_api_key', '');
  $params['domain'] = $_SERVER['SERVER_NAME'];
  $params['action'] = 'flush.custom';

  // Attach the obove info to each URL.
  foreach ($airee_urls as $url) {

    // Skips URLs which have already been purged.
    if (in_array($url, $url_cache)) {
      continue;
    }

    // Append absolute URL to data array.
    $absolute = url($url, array('alias' => 1, 'absolute' => 1));
    $prefixed = variable_get('airee_url_prefix', url('', array('absolute' => TRUE))) .
      str_replace(url('', array('absolute' => TRUE)), '', $url);

    // Append the URL to the purge queue.
    $params['pages'][] = $prefixed;

    // Save the URL to a static variable.
    $url_cache[] = $url;

    // Make sure page is cleared from local cache if enabled.
    if (variable_get('cache', 0)) {
      cache_clear_all($absolute, 'cache_page');
    }
  }

  // Get rid of blocks' content from Drupal's cache.
  if (count($urls) && module_exists('block') && variable_get('block_cache', 0)) {
    cache_clear_all('*', 'cache_block', 1);
  }

  // Check if requests should be handled in parallel.
  if (variable_get('airee_parallel', 0)) {
    return airee_parallel_request($params);
  }

  // Else use the less CPU intensive method.
  return airee_serial_request($params);
}


/**
 * Issue airee request using serial curl requests.
 *
 * @param array $urls
 *   Array of purge requests ready to be sent using curl.
 *
 * @return array
 *   Array of purge requests along with their responses after purging.
 */
function airee_serial_request($params) {

  // If no urls don't run.
  if (empty($params['pages'])) {
    return $params['pages'];
  }

  $api = variable_get('airee_api_url', 'http://xn--80aqc2a.xn--p1ai/my/site/api/');
  $params['pages'] = implode("\n", $params['pages']);
  $curl_airee = curl_init();

  curl_setopt($curl_airee, CURLOPT_URL, $api);
  curl_setopt($curl_airee, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl_airee, CURLOPT_POST, 1);
  curl_setopt($curl_airee, CURLOPT_POSTFIELDS, $params);
  curl_setopt($curl_airee, CURLOPT_TIMEOUT, 5);
  curl_setopt($curl_airee, CURLOPT_FOLLOWLOCATION, TRUE);
  curl_setopt($curl_airee, CURLOPT_AUTOREFERER, TRUE);
  curl_setopt($curl_airee, CURLOPT_SSL_VERIFYPEER, FALSE);

  $response = curl_exec($curl_airee);
  $body = array_pop(explode("\r\n\r\n", $response));

  $info = curl_getinfo($curl_airee);
  $urls[$key]['http_code'] = $info['http_code'];
  $urls[$key]['content'] = drupal_json_decode($body);

  return $urls;
}

/**
 * Issue airee request using parallel curl requests.
 *
 * @param array $urls
 *   Array of purge requests ready to be sent using curl.
 *
 * @return array
 *   Array of purge requests along with their responses after purging.
 */
function airee_parallel_request($params) {
  $api = variable_get('airee_api_url', 'http://xn--80aqc2a.xn--p1ai/my/site/api/');
  $mh = curl_multi_init();
  $curl_array = array();

  foreach ($params['pages'] as $i => $url) {
    $data = $params;
    $data['pages'] = $url;

    $curl_array[$i] = curl_init($api);
    curl_setopt($curl_array[$i], CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl_array[$i], CURLOPT_POSTFIELDS, $data);
    curl_multi_add_handle($mh, $curl_array[$i]);
  }
  $running = NULL;
  do {
    usleep(10000);
    curl_multi_exec($mh, $running);
  }
  while ($running > 0);

  $res = array();
  foreach ($urls as $i => $url) {
    $res[$url['url']] = curl_multi_getcontent($curl_array[$i]);
  }

  $info = curl_getinfo($curl_array[0]);

  if ($info['http_code'] != 200 || $info['download_content_length'] < 0) {
    watchdog('airee', 'There was a problem with CURL when trying to connect to
    @api_url. @error_message', array(
        '@api_url' => $api,
        '@error_message' => ucfirst(curl_error($curl_array[0])),
      )
    );
  }

  // Result collection. Collects the http code returned for each url purged.
  foreach ($urls as $i => $url) {
    $info = curl_getinfo($curl_array[$i]);
    $urls[$i]['http_code'] = $info['http_code'];
    $urls[$i]['content'] = drupal_json_decode(curl_multi_getcontent($curl_array[$i]));
    curl_multi_remove_handle($mh, $curl_array[$i]);
  }

  foreach ($params['pages'] as $i => $url) {
    curl_multi_remove_handle($mh, $curl_array[$i]);
  }
  curl_multi_close($mh);

  return $urls;
}


/**
 * Logs successful Airee purges and errors to the watchdog.
 *
 * @param array $responses
 *   Array of purge requests along with their responses.
 */
function airee_logging($responses) {
  $purges = array();
  $errors = array();
  foreach ($responses as $response) {
    if ($response['http_code'] == 200 && $response['content']['result'] == 'success') {
      $purges[] = $response['url'];
    }
    elseif ($response['http_code'] == 200 && $response['content']['result'] != 'success') {
      $errors[] = $response['url'] . ' -- ' . $response['content']['msg'];
    }
    else {
      $errors[] = 'Error, There was a problem connecting to Airee, here\'s the HTTP error code: ' . $response['http_code'];
    }
  }

  // Log number of succesful purges along with the URLs.
  if (count($purges)) {
    watchdog('airee', '!success_count URLs have been successfully purged from
Airee:<br /> @purge_log', array(
        '!success_count' => count($purges),
        '@purge_log' => implode('<br />', $purges),
      )
    );
  }

  // Log number of errors encountered along with the URLs.
  if (count($errors)) {
    watchdog('airee', '!errors_count errors have been encountered when purging
these URLs:<br /> @airee_log', array(
      '!errors_count' => count($errors),
      '@airee_log' => implode('<br />', $errors),
    ), WATCHDOG_ERROR
    );
  }
}
