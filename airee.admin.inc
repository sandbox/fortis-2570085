<?php


/**
 * Menu callback for airee admin settings.
 */
function airee_admin_settings_form() {

  $form = array();
  $form['airee_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Airee API URL'),
    '#default_value' => variable_get('airee_api_url', 'http://xn--80aqc2a.xn--p1ai/my/site/api/'),
    '#description' => t("Enter the URL for Airee's API, clear the field to use the default URL."),
    '#required' => 1,
  );
  $form['airee_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Airee API Key'),
    '#default_value' => variable_get('airee_api_key', ''),
    '#description' => t("Enter your key for Airee's API service. You'll find this under Account"),
    '#required' => 1,
  );
  $form['airee_parallel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send requests in parallel to API'),
    '#default_value' => variable_get('airee_parallel', 0),
    '#description' => t('Will use curl_multi intead of single requests. Faster but uses more CPU.'),
  );
  $form['airee_url_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('URL Prefix'),
    '#default_value' => variable_get('airee_url_prefix', url('', array('absolute' => TRUE))),
    '#description' => t('This converts the relative paths to absolute URLs using this prefix.'),
    '#required' => 1,
  );

  return system_settings_form($form);
}

/**
 * Validate the Purge settings.
 */
function airee_admin_settings_form_validate($form, &$form_state) {

  // Check if API URL is valid.
  if (!valid_url($form_state['values']['airee_api_url'], TRUE)) {
    form_set_error('airee_api_url', t('This is not a valid URL: !url.', array('!url' => $form_state['values']['airee_api_url'])));
  }

  // Check if path prefix is a valid URL.
  if (!valid_url($form_state['values']['airee_url_prefix'], TRUE)) {
    form_set_error('airee_api_url', t('This is not a valid URL: !url.', array('!url' => $form_state['values']['airee_api_url'])));
  }
}
