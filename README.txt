CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirement
 * Installation
 * Credits


INTRODUCTION
------------

Airee Purge is a plugin for the Expire module which enables it to
clear specific pages from the Airee CDN when you update, delete or add a
page.

Airee with "cache everything" enabled provides the ultimate performance for
your anonymous users, down to a 20 millisecond response time and extremely fast
transfer rate. When you enable this setting your site can instantly handle
millions of anonymous users, even if you're using a shared host.

By the way, Google also likes really fast websites, so it also improves your
SEO rating.


REQUIREMENT
-----------

An Airee account with enabled cache rules. Make sure
you created exclude rules for directories that you don't want to cache, such as
/services/service, in case you have a service set up using that path.

PHP with curl enabled. This module uses curl for issuing the http POST
requests to Airee.

Airee Purge requires the Expire module.


INSTALLATION
------------

If using Drush, drush dl airee and then drush en airee otherwise unpack,
place and enable just like any other module.

Navigate to Administration » Configuration » Development » Performance »
Airee Settings (admin/config/development/performance/airee) and
enter your Airee info.

Navigate to Administration » Configuration » Development » Performance
(admin/config/development/performance) and set the Expiration of Cached Pages to
3 hours or more. This means that Airee will store your page for 3 hours or
more if you don't edit or delete it beforehand. You can ignore the other
settings on this page. If you enable local caching--which wont be necessary
--your local cache will be purged as well.


NOTE
----

When using Airee all requests will come from one IP, to remedy this you
will have to add this line to the bottom of your settings.php file:
$_SERVER['REMOTE_ADDR'] = empty($_SERVER['HTTP_X_REAL_IP'])
  ? $_SERVER['REMOTE_ADDR']
  : $_SERVER['HTTP_X_REAL_IP'];


CREDIT
------

Alan Bondarchuk / fortis on Drupal.org

The Purge plugin, http://drupal.org/project/purge , for providing the
inspiration and guidance to develop this plugin.
